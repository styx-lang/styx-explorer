## Styx-explorer

Styx explorer is a fork of [compiler-explorer](https://github.com/compiler-explorer/compiler-explorer)
designed to explore [styx](https://gitlab.com/styx-lang/styx) backend production locally.

![](https://gitlab.com/styx-lang/styx-explorer/-/raw/styx/docs/images/styx-explorer.png)

## Build

See [official instruction](https://github.com/compiler-explorer/compiler-explorer), but essentially

```bash
git checkout styx
make
```

## Execute

```bash
git checkout styx
#EXTRA_ARGS being optional
make run-only EXTRA_ARGS='--language Styx'
```

## Notes

- requires the unit to be named `u`
- requires [styx](https://gitlab.com/styx-lang/styx) to be in the `$PATH`
