// Copyright (c) 2017, Compiler Explorer Authors
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

import path from 'path';

import { BaseCompiler } from '../base-compiler.js';

import type { PreliminaryCompilerInfo } from '../../types/compiler.interfaces.js';
import type { ParseFiltersAndOutputOptions } from '../../types/features/filters.interfaces.js';

export class StyxCompiler extends BaseCompiler {
    static get key() {
        return 'styx';
    }

    constructor(info: PreliminaryCompilerInfo, env) {
        super(info, env);
        this.compiler.supportsBinary = true;
        this.compiler.supportsIrView = true;
        this.compiler.irArg          = ['--femit-ir'];
    }

    override getSharedLibraryPathsAsArguments() {
        return [];
    }

    override buildExecutable(compiler, options, inputFilename, execOptions) {
        options = options.filter(param => param != '-o' && param != '-c');
        return this.runCompiler(compiler, options, inputFilename, execOptions);
    }

    override getOutputFilename(dirPath: string, outputFilebase: string, key?: any): string {
        if (!key || (key.filters && !key.filters.binary && !key.filters.binaryObject))
            return path.join(dirPath, '.styx', 'u.s');
        else if (key && key.filters && key.filters.binaryObject)
            return path.join(dirPath, '.styx', 'u.o');
        else
            return path.join(dirPath, 'u');
    }

    protected override optionsForFilter(filters: ParseFiltersAndOutputOptions, outputFilename: string, userOptions?: string[],): string[] {
        if (!filters.binary && !filters.binaryObject)
            return ['--be=asm', '--of=u.s', '-g'];
        else if (filters.binaryObject)
            return ['--be=obj', '--of=u.o', '-g'];
        else if (filters.binary)
            return ['--be=exe', '--of=u', '-g'];
        return [];
    }

    override getExecutableFilename(dirPath: string, outputFilebase: string, key?) {
        return path.join(dirPath, 'u');
    }

    override getIrOutputFilename(inputFilename: string, filters: ParseFiltersAndOutputOptions): string {
        return path.join(path.dirname(inputFilename), '.styx', 'u.ll');
    }
}
